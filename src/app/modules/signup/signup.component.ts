import { Component, OnInit } from "@angular/core";
import { FormGroup, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { AuthService } from "../auth/auth.service";
import { FormControl } from "@angular/forms";

@Component({
  selector: "app-signup",
  templateUrl: "./signup.component.html",
  styleUrls: ["./signup.component.css"]
})
export class SignupComponent implements OnInit {
  formGroup: FormGroup;
  public successfullySignup: boolean;
  errorMessage : string;
  isPasswordShow: boolean;



  constructor(
    private router: Router,
    private auth: AuthService
  ) {
    this.isPasswordShow = false;
  }

  ngOnInit() {
    this.formGroup = new FormGroup({
      Name: new FormControl("", [Validators.required]),
      BusinessEmail: new FormControl("", [
        Validators.required,
        Validators.pattern(
          /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/
        )
      ]),
      Password: new FormControl("", [
        Validators.required,
        Validators.minLength(8),
        Validators.maxLength(20)
      ]),
      AgreeCondition: new FormControl("", [
         Validators.required
      ])
    });
  }
  showPassword() {
    this.isPasswordShow = !this.isPasswordShow;
  }
  onSubmit() {
    const username = this.formGroup.value.Name, password = this.formGroup.value.Password ,email =this.formGroup.value.BusinessEmail;

    const user ={
      "username": username,
      "password": password,
      "attributes": {
        "email": email,
        "custom:role":'user' 
      }
    }
  
   this.auth.signUp(user)
      .subscribe(
        result => {
          this.successfullySignup = true;
          setTimeout(()=>{
          this.router.navigate(['/login']);
          },4000)
        },
        error => {
          this.errorMessage = error.message;
          console.log(error);
        });
  }

}
