import { Component, OnInit } from "@angular/core";
import { ApiService } from "../../../shared/service/api.service";
import { Router } from "@angular/router";
import {AppComponentService} from './home.component.service';

@Component({
  selector: "app-home",
  templateUrl: "./home.component.html",
  styleUrls: ["./home.component.css"],
  providers: [AppComponentService]
})
export class HomeComponent implements OnInit {
  featuredApps: any = [];
  toVerifyEmail: boolean;
  signstatus: string;
  froalaElements: any;
  public froalaEditors: any;
  public isEditMode: boolean = false;
  data: string;

//
  constructor(private apiService: ApiService,private appComponentService: AppComponentService, private route: Router) {
    this.froalaElements = this.appComponentService.getInitialElementsState();
    this.froalaEditors = this.appComponentService.getInitialEditorsState();
  }

  ngOnInit() {
    this.getFeaturedAppList();
    console.log("previous froala elements", JSON.stringify(this.froalaElements))
  }

  public getEditorByName(name: string): any {
    return this.froalaEditors[name];
  }
  public handleChangeEditMode(state): void {
    this.isEditMode = state;
    this.apiService.setData(state);

    if (!state) {
      this.froalaElements = this.appComponentService.getInitialElementsState();
    }
  }

  getFeaturedAppList() {
    this.apiService.getFeaturedApps().subscribe((data: any) => {
      this.featuredApps = data.list;
    });
  }

  appDetails(safeName: string) {
    this.route.navigate(["/app-details/" + safeName]);
  }
  public sendChanges() {
    console.log("Changes ", JSON.stringify(this.froalaElements))
    this.callMethod();
  }
  callMethod(){
    this.apiService.getUpdatedFroalaElement.subscribe((data) =>{
      this.data = data
      console.log("Inside home component data", this.data)
      var merged = Object.assign(this.froalaElements, this.data);
      console.log("merged data", JSON.stringify(merged))
    this.appComponentService.sendChanges(merged);

    })
  }
}
