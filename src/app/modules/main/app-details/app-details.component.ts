import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { ApiService } from "src/app/shared/service/api.service";
import { DomSanitizer } from "@angular/platform-browser";

@Component({
  selector: "app-app-details",
  templateUrl: "./app-details.component.html",
  styleUrls: ["./app-details.component.css"]
})
export class AppDetailsComponent implements OnInit {
  safeName: string;
  appDetails: any = [];
  description: any = [];
  categories: any = [];
  appData: string;
  images: any = [];
  showModal: boolean = false;
  imageObject: any = [];
  logo: string;
  constructor(
    private activatedRoute: ActivatedRoute,
    private apiService: ApiService,
    private sanitizer: DomSanitizer,
    private route: Router
  ) {}

  ngOnInit() {
    this.activatedRoute.params.subscribe(params => {
      this.safeName = params["safeName"];
    });
    this.getAppDetailsbySafeName(this.safeName);
  }

  getAppDetailsbySafeName(safeName: string) {
    this.apiService.getAppDetails(safeName).subscribe((data: any) => {
      this.appDetails = data;
      this.description = data.customData.description;
      this.categories = data.customData.categories;
      this.logo = data.customData.logo;
      data.customData.images.forEach(element => {
        const name = element;
        const lastDot = name.lastIndexOf(".");
        const ext = name.substring(lastDot + 1);
        this.imageObject = {
          imagePath: element,
          imageExt: ext
        };
        this.images.push(this.imageObject);
      });
      this.images.push({
        imagePath: this.sanitizer.bypassSecurityTrustResourceUrl(
          data.customData["video-url"]
        ),
        imageExt: "video"
      });
    });
  }

  openModal() {
    this.appData = this.appDetails;
    this.showModal = true;
  }

  goToHome() {
    this.route.navigate(["/"]);
  }
}
