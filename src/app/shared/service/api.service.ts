import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { environment } from "../../../environments/environment";
import {
  DomSanitizer,
  SafeResourceUrl,
  SafeUrl
} from "@angular/platform-browser";
import { BehaviorSubject, Observable } from "rxjs";

import { ConstantClass } from "../constant/const";

@Injectable({
  providedIn: "root"
})
export class ApiService {
  baseUrl: string = environment.baseUrl;

  $searchQuerySubject = new BehaviorSubject({});
  $selectedOptionSubject = new BehaviorSubject({});
  private _data: BehaviorSubject<any> = new BehaviorSubject<any>(null);


  public setData(data: any){
    this._data.next(data);
}

public getData(): Observable<any> {
    return this._data.asObservable();
}

private _updateFroalaElements = new BehaviorSubject<any>(null);
getUpdatedFroalaElement = this._updateFroalaElements.asObservable();
updateFroalaElement(message: string) {
this._updateFroalaElements.next(message)
}

  encodeFeaturedAppsQueryUrl = encodeURIComponent(
    ConstantClass.FEATURED_APP_QUERY_URL
  );
  encodeFeaturedAppsSortUrl = encodeURIComponent(
    ConstantClass.FEATURED_APP_SORT_URL
  );
  encodePopularAppsQueryUrl = encodeURIComponent(
    ConstantClass.POPULAR_APPS_QUERY_URL
  );
  encodePopularAppsSortUrl = encodeURIComponent(
    ConstantClass.POPULAR_APPS_SORT_URL
  );
  encodeNewestAppsQueryUrl = encodeURIComponent(
    ConstantClass.NEWEST_APPS_QUERY_URL
  );
  encodeNewestAppsSortUrl = encodeURIComponent(
    ConstantClass.NEWEST_APPS_SORT_URL
  );
  encodeSearchAppsQuesyUrl = encodeURIComponent(
    ConstantClass.SEARCH_APPS_QUERY_URL
  );
  encodeSearchAppsFielsdsUrl = encodeURIComponent(
    ConstantClass.SEARCH_APPS_FILE_URL
  );
  encodeAnalyticsAppsQueryUrl = encodeURIComponent(
    ConstantClass.ANALYTICS_APPS_QUERY_URL
  );
  encodeCommunicationAppsQueryUrl = encodeURIComponent(
    ConstantClass.COMMUNICATION_APPS_QUERY_URL
  );
  encodeCustomerSupportAppsQueryUrl = encodeURIComponent(
    ConstantClass.CUSTOMER_SUPPORT_APPS_QUERY_URL
  );
  encodeFileManagementAppsQueryUrl = encodeURIComponent(
    ConstantClass.FILE_MANAGEMENT_APPS_QUERY_URL
  );
  encodeProductivityAppsQueryUrl = encodeURIComponent(
    ConstantClass.PRODUCTIVITY_APPS_QUERY_URL
  );
  encodeDeveloperToolsAppsQueryUrl = encodeURIComponent(
    ConstantClass.DEVELOPER_TOOLS_APP_QUERY_URL
  );

  constructor(private http: HttpClient, private sanitizer: DomSanitizer) {}

  public getSelectedItemApps(selectedItem) {
    switch (selectedItem) {
      case "All Apps":
        return this.http.get(`${this.baseUrl}apps`);
      case "Featured":
        return this.http.get(
          `${this.baseUrl}apps?query=${this.encodeFeaturedAppsQueryUrl} &sort=${this.encodeFeaturedAppsSortUrl}&limit=4`
        );
      case "Popular":
        return this.http.get(
          `${this.baseUrl}apps?query=${this.encodePopularAppsQueryUrl}&sort=${this.encodePopularAppsSortUrl}&limit=6`
        );
      case "Newest":
        return this.http.get(
          `${this.baseUrl}apps?query=${this.encodeNewestAppsQueryUrl}&sort=${this.encodeNewestAppsSortUrl}&limit=6`
        );
      case "Analytics":
        return this.http.get(
          this.baseUrl + "apps?query=" + this.encodeAnalyticsAppsQueryUrl
        );
      case "Communication":
        return this.http.get(
          this.baseUrl + "apps?query=" + this.encodeCommunicationAppsQueryUrl
        );
      case "Customer Support":
        return this.http.get(
          this.baseUrl + "apps?query=" + this.encodeCustomerSupportAppsQueryUrl
        );
      case "File Management":
        return this.http.get(
          this.baseUrl + "apps?query=" + this.encodeFileManagementAppsQueryUrl
        );
      case "Productivity":
        return this.http.get(
          this.baseUrl + "apps?query=" + this.encodeProductivityAppsQueryUrl
        );
      case "Developer Tools":
        return this.http.get(
          this.baseUrl + "apps?query=" + this.encodeDeveloperToolsAppsQueryUrl
        );
      default: {
        break;
      }
    }
    // return selectedItem
  }

  // public getAllApps() {
  //   return this.http.get(`${this.baseUrl}apps`);
  // }

  public getFeaturedApps() {
    return this.http.get(
      `${this.baseUrl}apps?query=${this.encodeFeaturedAppsQueryUrl} &sort=${this.encodeFeaturedAppsSortUrl}&limit=4`
    );
  }

  public getPopularApps() {
    return this.http.get(
      this.baseUrl +
        "apps?query=" +
        this.encodePopularAppsQueryUrl +
        "&sort=" +
        this.encodePopularAppsSortUrl +
        "&limit=6"
    );
  }

  public getNewestApps() {
    return this.http.get(
      this.baseUrl +
        "apps?query=" +
        this.encodeNewestAppsQueryUrl +
        "&sort=" +
        this.encodeNewestAppsSortUrl +
        "&limit=6"
    );
  }

  // public getAnalyticsApps() {
  //   return this.http.get(
  //     this.baseUrl + "apps?query=" + this.encodeAnalyticsAppsQueryUrl
  //   );
  // }

  // public getCommunicationApps() {
  //   return this.http.get(
  //     this.baseUrl + "apps?query=" + this.encodeCommunicationAppsQueryUrl
  //   );
  // }

  // public getCustomerSupportApps() {
  //   return this.http.get(
  //     this.baseUrl + "apps?query=" + this.encodeCustomerSupportAppsQueryUrl
  //   );
  // }

  // public getFileManagementApps() {
  //   return this.http.get(
  //     this.baseUrl + "apps?query=" + this.encodeFileManagementAppsQueryUrl
  //   );
  // }

  // public getProductivityApps() {
  //   return this.http.get(
  //     this.baseUrl + "apps?query=" + this.encodeProductivityAppsQueryUrl
  //   );
  // }

  // public getDeveloperToolsApps() {
  //   return this.http.get(
  //     this.baseUrl + "apps?query=" + this.encodeDeveloperToolsAppsQueryUrl
  //   );
  // }

  public getSearchApp(searchTextaValue) {
    return this.http.get(
      this.baseUrl +
        "apps/textSearch?query=" +
        this.encodeSearchAppsQuesyUrl +
        "&text=" +
        searchTextaValue +
        "&fields=" +
        this.encodeSearchAppsFielsdsUrl
    );
  }

  public getCollectionCategoryfilters() {
    return this.http.get(this.baseUrl + "frontEnd/filters");
  }

  public getAppDetails(safeName: string) {
    return this.http.get(this.baseUrl + "apps/bySafeName/" + safeName);
  }

  public getFileDownlaodURL(binary: string) {
    return this.http.get(this.baseUrl + "files/download/?fileId=" + binary);
  }
}
